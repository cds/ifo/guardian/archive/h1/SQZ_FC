# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_FC.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_FC.py $
# to reset all FC settings for FC-IR IFO locking, manual through "INIT"

from guardian import GuardState, GuardStateDecorator, NodeManager
from guardian.state import TimerManager
from timeout_utils import call_with_timeout

import ISC_GEN_STATES

import time
import cdsutils as cdu
import numpy as np
import math
import json

import sqzparams

nominal = 'IR_LOCKED'
request = 'DOWN'

fcParamsPath = '/opt/rtcds/userapps/release/sqz/h1/guardian/fcParams.dat'
with open(fcParamsPath, 'r') as fcParamFile:
    fcParamDict = json.load(fcParamFile)

fcgsOffsetList = fcParamDict['fcOffsets']

##################################################
# NODES
##################################################

nodes = NodeManager(['SUS_FC1',
                     'SUS_FC2',
                     ])

##################################################
# Functions
##################################################
def GR_locked():
    # Factor of 1e3 : TRANS_C measured uW, REFL_DC_POWER measures mW
    # Factor of 2 : FC REFL is 50% of FCGS fiber output power
    # Factor of 0.3 : trigger on 30% TEM00 mode-matching in case FC is misaligned
    #fcgs_trans_lock_threshold = sqzparams.fcgs_trans_lock_threshold
    #fcgs_trans_lock_threshold = ezca['SQZ-FC_REFL_DC_POWER']*1e3 * 2 * 0.3
    return ezca['SQZ-FC_TRANS_C_LF_OUTPUT'] > sqzparams.fcgs_trans_lock_threshold
    #return ezca['SQZ-FC_TRANS_C_DC_NORMALIZED'] > 0.7


def IR_locked():
    #fcWFS_qDip_lock_threshold = -500
    return ezca['SQZ-FC_WFS_A_Q_SUM_OUTPUT'] < sqzparams.fcWFS_qDip_lock_threshold

def IR_near_res():
    flag = False
    [i_avg,q_avg] = cdu.avg(-1,['H1:SQZ-FC_WFS_A_I_SUM_OUT_DQ','H1:SQZ-FC_WFS_A_Q_SUM_OUT_DQ'], stddev = True)
    log('fc_wfs_q avg = %f'%( q_avg[0]))
    log('fc_fws_i std = %f'%( i_avg[1]))
    if q_avg[0] < -100 or abs(i_avg[1]) > 800:
        flag = True
    if flag: log('near ir resonance!')
    if not flag: log('not close enough to IR?')
    return flag

def check_FC2_sus_rms():
    fc2_sus_check_rms = cdu.avg(1,'SUS-FC2_M1_DAMP_L_IN1_DQ', stddev = True)[1]
    return (fc2_sus_check_rms < 0.1)

def FC2_M3_saturated():  #Check M3 saturations
    flag=False
    #fc2_sus_m3 = cdu.avg(0.2,['FEC-166_DAC_OUTPUT_1_4','FEC-166_DAC_OUTPUT_1_5','FEC-166_DAC_OUTPUT_1_6','FEC-166_DAC_OUTPUT_1_7'])
    fc2_sus_m3 = [ezca['FEC-166_DAC_OUTPUT_1_4'], ezca['FEC-166_DAC_OUTPUT_1_5'], ezca['FEC-166_DAC_OUTPUT_1_5'], ezca['FEC-166_DAC_OUTPUT_1_5']]
    if all(abs(counts)>5e5 for counts in fc2_sus_m3):
        flag=True
        notify('FC2 M3 saturated!')
    return flag

def TTFSS_locked():
    if (abs(ezca['SQZ-FIBR_LOCK_BEAT_FREQUENCY']-160)<0.01) or ezca.read('GRD-SQZ_FC_TARGET_S', as_string=True)=='IR_VCO_LOCKED':
        return True
    else:
        return False

def check_TEM00():
    flag = False
    w0 = 105
    log('checking mode on camera')
    if (ezca.read('VID-FC_TRANS_C_WX', as_string=True)!='nan') and (ezca.read('VID-FC_TRANS_C_WY', as_string=True)!='nan'):
        wx = ezca['VID-FC_TRANS_C_WX']
        wy = ezca['VID-FC_TRANS_C_WY']
        log(f'wx={wx} and wy={wy}')
        if (wx<w0 and wy<w0 and wx>w0/2 and wy>w0/2):
            log('looks like TEM00!')
            flag = True
        else: notify('not TEM00?')
    return flag

# for scanning IR/GR VCO tune voltage offset
def scan(startoffs,endoffs,volts_stepSize=0.008):
    ezca['SQZ-FC_VCO_TUNEOFS']=startoffs
    time.sleep(2)
    start_time = ezca['DAQ-DC0_GPS']
    while(ezca['SQZ-FC_VCO_TUNEOFS']<endoffs):
        ezca['SQZ-FC_VCO_TUNEOFS']+=volts_stepSize
        time.sleep(1/16)
    end_time = ezca['DAQ-DC0_GPS']
    return [start_time,end_time]

def find_min(times):  # find minimum q-dip and corresponding vco offset from the scan times
    start_time,end_time = times
    log('getting nds data, cdu.getdata(), in find_min function..')
    [WFS,VCO] = call_with_timeout(cdu.getdata, ['H1:SQZ-FC_WFS_A_Q_SUM_OUT_DQ','H1:SQZ-FC_VCO_TUNEOFS'], end_time-start_time, start_time)
    WFS = WFS.data
    VCO = VCO.data
    # find minimum q-dip and corresponding vco offset
    min_frac = np.argmin(WFS)/len(WFS)
    log('min found at this fraction of the scan = %f'%(min_frac))
    min_time = start_time+(end_time-start_time)*min_frac
    # set VCO green/IR co-resonance offset to near minimum
    ezca['SQZ-FC_VCO_TUNEOFS'] = (np.max(VCO)-np.min(VCO))*min_frac*0.9+np.min(VCO)

    return np.min(WFS)

##################################################
# Decorator
##################################################
class GR_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not GR_locked():
            log('GR lost lock??')
            return 'DOWN'
        if abs(ezca['SQZ-FC_VCO_TUNEOFS'])>9.5:
            notify('FC VCO railing?')
            return 'DOWN'

class IR_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not IR_locked():
            log('IR lost lock')
            #return 'DOWN'

class TTFSS_checker(GuardStateDecorator):
    def pre_exec(self):
        if not TTFSS_locked():
            log('TTFSS unlocked?')
            #return 'DOWN'

##################################################
# STATES
##################################################
class INIT(GuardState):   ## INIT RESETS ALL FC SETTINGS FOR FC-IR LOCKING ON THE IFO
    index = 0
    request = True

    def main(self):
        # reset FC CMB
        ezca['SQZ-FC_SERVO_IN1POL'] = 'Off' #FOR GR VCO
        #ezca['SQZ-FC_SERVO_IN1POL'] = 'On' #FOR IR VCO
        ezca['SQZ-FC_SERVO_FASTPOL'] = 'Off'
        ezca['SQZ-FC_SERVO_COMBOOST'] = 0
        ezca['SQZ-FC_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-FC_SERVO_COMCOMP'] = 'On'
        ezca['SQZ-FC_SERVO_FASTGAIN'] = 0

        # reset FC-LSC inmatrix
        for ii in range(13):
            for jj in range(4):
                ezca['SQZ-FC_LSC_INMTRX_SETTING_%d_%d'%(jj+1,ii+1)] = 0
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 0 #self.TIMEOUT_T
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1

        #reset FC-LSC outmatrix
        for ii in range(3):
            for jj in range(4):
                ezca['SQZ-FC_LSC_OUTMTRX_%d_%d'%(ii+1,jj+1)] = 0
        ezca['SQZ-FC_LSC_OUTMTRX_2_1'] = 1 # DOF1 (GR_SLW) --> FC2
        ezca['SQZ-FC_LSC_OUTMTRX_2_2'] = 1 # DOF2 (IR_WFS_A_I) --> FC2
        ezca.get_LIGOFilter('SUS-FC2_M1_LOCK_L').only_on('FM1','FM5','FM7','FM8','FM9','FM10','LIMIT','INPUT','OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('SUS-FC2_M3_LOCK_L').switch_on('INPUT','OUTPUT')

        # disable FC LSC DITHER LOCKING
        # (which is used sometimes b/c laser VCO feedback goes through the dither servo output, before its DAC output)
        ezca['SQZ-FC_LSC_FREQ_GAIN'] = 0
        ezca.get_LIGOFilter('SQZ-FC_LSC_FREQ').switch_off('INPUT','OUTPUT')
        ezca.get_LIGOFilter('SQZ-FC_LSC_DITHER_DITHER').switch_off('INPUT','OUTPUT')
        ezca.get_LIGOFilter('SQZ-FC_LSC_DITHER_SERVO').switch_off('INPUT','OUTPUT')

        #reset outmatrix
        ezca['SQZ-FC_LSC_OUTMTRX_2_1'] = 1 # DOF1 (GR_SLOW) --> FC2
        ezca['SQZ-FC_LSC_OUTMTRX_2_2'] = 1 # DOF2 (IR_WFS_SUM) --> FC2

        #log('Initializing subordinate nodes:')
        #nodes.set_managed()
    def run(self):
        return True


class IDLE(GuardState):
    index = 10
    request = True

    #@TTFSS_checker
    def main(self):
        # set dof gains to 1, use inmatrx to ramp gains
        for ii in range(2):
            dof = 'DOF%d'%(ii+1)
            ezca['SQZ-FC_LSC_%s_TRAMP'%dof] = 0
            ezca['SQZ-FC_LSC_%s_GAIN'%dof] = 1

        for ii in range(2):
            dof = 'DOF%d'%(ii+1)
            ezca['SQZ-FC_LSC_%s_TRAMP'%dof] = 2

        # set inmatrix ramp time
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 2

        # clear slowcontrols vco integrator
        ezca['SQZ-FC_VCO_CONTROLS_CLEARINT'] = 1
        time.sleep(0.2)
        ezca['SQZ-FC_VCO_CONTROLS_CLEARINT'] = 0

        # check FC2 SUS GRD is "ALIGNED"
        self.flag_fc2aligned = True
        if ezca['GRD-SUS_FC2_STATE_S'] != 'ALIGNED':
            notify('FC2 NOT ALIGNED?')
            nodes['SUS_FC2'] = 'ALIGNED'
            self.flag_fc2aligned = False

        ezca['SQZ-FC_VCO_TUNEOFS'] = fcParamDict['vcoOffsets'][0]

    #@TTFSS_checker
    def run(self):
        if not self.flag_fc2aligned:
            # check FC2 SUS is at "ALIGNED"
            if nodes['SUS_FC2'].arrived and nodes['SUS_FC2'].done:
                # once FC2 SUS is stable, go on to VCO locking
                if check_FC2_sus_rms():
                    return True
                else:
                    notify('waiting for FC2 sus')
        else:
            return True


#####  OFFLOAD FC-ASC TO ZM3 AND FC1+2  #####
OFFLOAD_FC_ASC = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('SQZ_FC_ASC', 5, ['FC1', 'FC2', 'ZM3'])
OFFLOAD_FC_ASC.index = 5

# vx 10/17 re-doing this with what's in the script userapps/sqz/h1/scripts/ASC/offload_FC_ASC, same as FC-ASC offload button.
'''class OFFLOAD_FC_ASC(GuardState):
    index = 5
    request = False
    def main(self):
        log('getting new P/Y values with ASC offsets')
        # get current ZM P/Y vals, and the ASC P/Y offsets
        POSp = ezca['SQZ-FC_ASC_CAV_POS_P_OUTPUT']
        POSy = ezca['SQZ-FC_ASC_CAV_POS_Y_OUTPUT']
        ANGp = ezca['SQZ-FC_ASC_CAV_ANG_P_OUTPUT']
        ANGy = ezca['SQZ-FC_ASC_CAV_ANG_Y_OUTPUT']
        INJ_ANGp = ezca['SQZ-FC_ASC_INJ_ANG_P_OUTPUT']
        INJ_ANGy = ezca['SQZ-FC_ASC_INJ_ANG_Y_OUTPUT']
        INJ_POSp = ezca['SQZ-FC_ASC_INJ_POS_P_OUTPUT']
        INJ_POSy = ezca['SQZ-FC_ASC_INJ_POS_Y_OUTPUT']
        time.sleep(1)
        log('old asc outputs:')
        log(f'POSp,POSy,ANGp,ANGy,INJ_ANGp,INJ_ANGy={POSp,POSy,ANGp,ANGy,INJ_ANGp,INJ_ANGy}')

        FC1p = ezca['SUS-FC1_M1_OPTICALIGN_P_OFFSET'] + (POSp+ANGp)/ezca['SUS-FC1_M1_OPTICALIGN_P_GAIN']
        FC1y = ezca['SUS-FC1_M1_OPTICALIGN_Y_OFFSET'] + (POSy+ANGy)/ezca['SUS-FC1_M1_OPTICALIGN_Y_GAIN']
        FC2p = ezca['SUS-FC2_M1_OPTICALIGN_P_OFFSET'] + (POSp-ANGp)/ezca['SUS-FC2_M1_OPTICALIGN_P_GAIN']
        FC2y = ezca['SUS-FC2_M1_OPTICALIGN_Y_OFFSET'] + (-POSy+ANGy)/ezca['SUS-FC2_M1_OPTICALIGN_Y_GAIN']
        ZM3p = ezca['SUS-ZM3_M1_OPTICALIGN_P_OFFSET'] + INJ_ANGp/ezca['SUS-ZM3_M1_OPTICALIGN_P_GAIN']
        ZM3y = ezca['SUS-ZM3_M1_OPTICALIGN_Y_OFFSET'] + INJ_ANGy/ezca['SUS-ZM3_M1_OPTICALIGN_Y_GAIN']
        ZM2p = ezca['SUS-ZM2_M1_OPTICALIGN_P_OFFSET'] + INJ_POSp/ezca['SUS-ZM2_M1_OPTICALIGN_P_GAIN']
        ZM2y = ezca['SUS-ZM2_M1_OPTICALIGN_Y_OFFSET'] + INJ_POSy/ezca['SUS-ZM2_M1_OPTICALIGN_Y_GAIN']

        # set FC 1/2 + ZM3 sliders to calculated values for the offload
        ezca['SUS-FC1_M1_OPTICALIGN_P_OFFSET']=FC1p
        ezca['SUS-FC2_M1_OPTICALIGN_P_OFFSET']=FC2p
        ezca['SUS-ZM3_M1_OPTICALIGN_P_OFFSET']=ZM3p
        ezca['SUS-ZM2_M1_OPTICALIGN_P_OFFSET']=ZM2p

        # set FC 1/2 + ZM3 sliders to calculated values for the offload
        ezca['SUS-FC1_M1_OPTICALIGN_Y_OFFSET']=FC1y
        ezca['SUS-FC2_M1_OPTICALIGN_Y_OFFSET']=FC2y
        ezca['SUS-ZM3_M1_OPTICALIGN_Y_OFFSET']=ZM3y
        ezca['SUS-ZM2_M1_OPTICALIGN_Y_OFFSET']=ZM2y
        return True
'''

# graceful clear history of FC-ASC after offloading sliders
class CLEAR_FC_ASC(GuardState):
    index = 6
    request = False
    def main(self):
        log('gracefully clearing FC-ASC outputs')
        for dof_in in ['CAV','INJ']:
            for dof_out in ['POS','ANG']:
                for py in ['P', 'Y']:
                    ezca.get_LIGOFilter('SQZ-FC_ASC_%s_%s_%s'%(dof_in,dof_out,py)).ramp_gain(0, ramp_time=5, wait=False)
        time.sleep(5)
        for dof_in in ['CAV','INJ']:
            for dof_out in ['POS','ANG']:
                for py in ['P', 'Y']:
                    ezca['SQZ-FC_ASC_%s_%s_%s_RSET'%(dof_in,dof_out,py)] = 2
                    ezca.get_LIGOFilter('SQZ-FC_ASC_%s_%s_%s'%(dof_in,dof_out,py)).ramp_gain(1, ramp_time=1, wait=False)
        log('done clearing FC ASC!')
        return True

class FC_ASC_OFFLOADED(GuardState):
    index = 7
    request = True
    def run(self):
        return True


#####
class DOWN(GuardState):
    index = 2
    request = True
    goto = True

    def main(self):
        self.TIMEOUT_T = 5
        self.counter = 0

        # ramp off output, for FC2 M1 integrator
        ezca['SUS-FC2_M1_LOCK_L_TRAMP'] = self.TIMEOUT_T
        ezca['SUS-FC2_M1_LOCK_L_GAIN'] = 0

        # ramp off output FC_LSC DOF filter banks
        for ii in range(4):
            dof = 'DOF%d'%(ii+1)
            ezca['SQZ-FC_LSC_%s_TRAMP'%dof] = self.TIMEOUT_T
            ezca['SQZ-FC_LSC_%s_GAIN'%dof] = 0
            ezca['SQZ-FC_LSC_%s_RSET'%dof] = 2  # vx 4/15 moved up

        # zero the inmatrix to sus feedback
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 0
        for ii in range(13):
            for jj in range(4):
                ezca['SQZ-FC_LSC_INMTRX_SETTING_%d_%d'%(jj+1,ii+1)] = 0
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1

        # configure FC-ASC for beam spot centering ADS
        # dither 1=FC1(pos), 2=FC2(ang). ADS 1,2 demods sent to ZM1(pos) and ZM3(ang).
        for py in ['PIT', 'YAW']:
            for dof in range(2):
                # Turning off dithers when not using ads
                ezca['SQZ-FC_ASC_ADS_%s%s_OSC_CLKGAIN'%(py,dof+1)] = 0

        self.timer['wait'] = self.TIMEOUT_T
        log('ramping all FC-LSC DOF/SUS outputs to 0')

        # disable FC ASC
        log('disabling FC ASC')
        ezca['SQZ-FC_ASC_SWITCH'] = 0

        # disable + reset FCGS CMB servo
        ezca['SQZ-FC_SERVO_COMBOOST'] = 0
        ezca['SQZ-FC_SERVO_IN1EN'] = 'Off'

        # disable FCGS VCO
        ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 0
        log('done clearing')

    def run(self):

        if self.counter == 0:
            # clear integrator histories once outputs are zero'd after 'wait' timer
            if self.timer['wait']:
                # clear histories on FC_LSC filter banks
                for ii in range(4):
                    dof = 'DOF%d'%(ii+1)
                    ezca['SQZ-FC_LSC_%s_RSET'%dof] = 2

                # clear histories on FC2 integrators
                ezca['SUS-FC2_M1_LOCK_L_RSET'] = 2
                ezca['SUS-FC2_M3_LOCK_L_RSET'] = 2

                # reset FC_LSC boosts
                ezca.switch('SQZ-FC_LSC_DOF1','FM1','OFF','FM2','OFF') #disable GR sus boost
                ezca.switch('SQZ-FC_LSC_DOF2','FM1','OFF','FM2','OFF','FM3','OFF','FM9','OFF') #disable IR sus boost

                self.counter += 1

        elif self.counter == 1:
            if check_FC2_sus_rms():
                osem_inmons = np.array(cdu.avg(0.5,['H1:SUS-FC2_M3_OSEMINF_UL_INMON','H1:SUS-FC2_M3_OSEMINF_UR_INMON','H1:SUS-FC2_M3_OSEMINF_LL_INMON','H1:SUS-FC2_M3_OSEMINF_LR_INMON'], stddev = True))
                if all(osem_inmons[:,1]<50):
                    log(f'FC2 M3 osems ready, stds [UL,UR,LL,LR]={np.round(osem_inmons[:,1],3)}')
                    self.counter+=1
                else:
                    log(f'waiting for FC2 SUS, M3 osem stds [UL,UR,LL,LR]={osem_inmons[:,1]}')
            else:
                notify('waiting for FC2 SUS')

        elif self.counter == 2:
            return True

class GR_VCO_LOCKING(GuardState):
    index = 50
    request = False

    #@TTFSS_checker
    def main(self):
        self.SUPERTIMEOUT_T = 45
        self.TIMEOUT_T = 1  #toggle time

        ezca['SQZ-FC_SERVO_IN1GAIN'] = sqzparams.fc_cmb_in1_gain

        # reset FC GR (DOF1) and IR (DOF2) filter banks
        ezca.get_LIGOFilter('SQZ-FC_LSC_DOF1').only_on('FM3','FM4','FM5','DECIMATION')
        ezca.get_LIGOFilter('SQZ-FC_LSC_DOF2').only_on('FM1','FM2','FM3','FM4','FM5','FM6','FM9','FM10','INPUT','OUTPUT','DECIMATION')

        self.timer['toggle_timeout'] = self.SUPERTIMEOUT_T
        self.timer['timeout'] = self.TIMEOUT_T
        self.counter = 0

    #@TTFSS_checker
    def run(self):
        if GR_locked(): #if green trans is good, check if we're on 00, otherwise toggle fcgs
            return True

        # return 'DOWN' if toggling fails for too long (SUPERTIMEOUT_T)
        if self.timer['toggle_timeout']:
            return 'DOWN'

        if self.counter == 0:
            if not ezca['SQZ-FC_FIBR_FLIPPER_READBACK']:
                notify('FCGS flipper closed')
                time.sleep(1)
            else:
                self.counter += 1

        # toggle the CMB input on/off to lock fcgs on tem00
        elif self.counter == 1:
            # make sure sus has settled
            if check_FC2_sus_rms():
                self.counter += 1
            else:
                notify('waiting for FC2 sus')

        elif self.counter == 2:
            ezca['SQZ-FC_SERVO_IN1EN'] = 'On'  # toggle on
            self.timer['timeout'] = self.TIMEOUT_T+1
            self.counter += 1

        elif self.counter == 3:
            if GR_locked():  #if green locked w/vco, move on to check camera for tem00
                self.timer['timeout'] = self.TIMEOUT_T
                self.counter += 1
            elif self.timer['timeout']: #else toggle fcgs vco servo
                log('timeout... Toggling servo.')
                ezca['SQZ-FC_SERVO_IN1EN'] = 'Off' # toggle off
                self.counter = 1

        elif self.counter == 4:
            if GR_locked(): #if green trans is good, check if we're on 00, otherwise toggle fcgs
                if self.timer['timeout']:
                    return True
            else:
                self.counter = 1


class GR_SUS_LOCKING(GuardState):
    index = 70
    request = False

    @GR_lock_checker
    #@TTFSS_checker
    def main(self):
        ezca['SUS-FC2_M1_LOCK_L_TRAMP'] = 1
        ezca['SUS-FC2_M1_LOCK_L_GAIN'] = -0.2
        ezca.switch('SQZ-FC_LSC_DOF1','INPUT','ON','OUTPUT','ON')

        self.TIMEOUT_T = 3
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = self.TIMEOUT_T
        self.GR_GAIN = sqzparams.fc_green_sus_gain
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = self.GR_GAIN
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1

        self.timer['timeout'] = 15
        self.timer['pause'] = self.TIMEOUT_T
        self.counter = 0

    @GR_lock_checker
    #@TTFSS_checker
    def run(self):

        if self.counter == 0:
            # boost once sus settles
            if check_FC2_sus_rms():
                if self.timer['pause']:
                    notify('engaging sus boost')
                    ezca.switch('SQZ-FC_LSC_DOF1','FM1','ON','FM2','ON') #GR SUS boost once sus settles
                    self.counter += 1
            else:
                notify('waiting on FC2 sus to engage boost')

        elif self.counter == 1:
            if GR_locked():
                return True
            else:
                return 'DOWN'

        if self.timer['timeout']:
            return 'DOWN'


class GR_LOCKED(GuardState):
    index = 100
    request = True
    def main(self):
        self.timer['pause'] = 1
        self.counter=0

    @GR_lock_checker
    def run(self):
        if self.counter == 0 and self.timer['pause']: # 1) check if TEM00
            if not check_TEM00():
                notify('not TEM00! Adjust FC2 P/Y, clear FC-ASC history?')
                return 'DOWN'
            else: self.counter += 1

        elif self.counter == 1:  # 2) check if FC2 SUS is quiet
            # monitor fc2 sus
            if not check_FC2_sus_rms():
                notify('FC2 SUS NOISY!')
            else:
                self.counter += 1

        elif self.counter == 2:
            return True


################################################################
#############      HANDOFF TO IR LOCKING     ###################
################################################################
class FIND_IR(GuardState):
    index = 200
    request = False

    @GR_lock_checker
    def main(self):
        self.counter = -1
        self.scanWidth = 0.2
        self.timer['pause']=0
        self.threshHigh = 0
        ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 0

        self.timer['timeout']=90
        # start to find_ir from closest freq offset
        self.currFreqOffset = ezca['SQZ-FC_VCO_CONTROLS_SETFREQUENCYOFFSET']
        self.idx = np.argmin([abs(freq-self.currFreqOffset) for freq in fcgsOffsetList])

    @GR_lock_checker
    def run(self):
        if self.timer['timeout']:
            return 'GR_LOCKED'

        if not ezca['SQZ-CLF_FLIPPER_READBACK']: #this state only checks for RLF/CLF beatnote
            notify('clf flipped closed')
            return 'GR_LOCKED'

        if FC2_M3_saturated(): #5/4 - check for FC2 M3 saturations
            return 'DOWN'

        if self.counter == -1 and self.timer['pause']: #if close enough, go to fine_tune_ir
            if IR_near_res():
                ezca['SQZ-FC_VCO_TUNEOFS']-=0.007
                return True
            else:
                self.counter += 1

        if self.counter >= len(fcgsOffsetList): #if tried all in list, it's not near a known co-res freq
            log('no rlf near known offsets')
            ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 0
            return 'GR_LOCKED'

        else: # try 2 known RLF frequency offsets in fcgsOffsetList
            #fc unlocks if we jump the vco volts, use slowcontrols servo to slowly jump freq
            ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 1
            log('attempt #%i'%(self.counter))
            log('going to %f Hz'%(fcgsOffsetList[self.idx]))  #4/7
            ezca['SQZ-FC_VCO_CONTROLS_SETFREQUENCYOFFSET'] = fcgsOffsetList[self.idx] #4/7
            time.sleep(1)
            while (abs(ezca['SQZ-FC_VCO_CONTROLS_DIFFFREQUENCY']) > 3000): #wait for vco servo
                if self.timer['timeout']:
                    return 'GR_LOCKED'
                else:
                    continue

            #then disable vco servo and do local find_ir course scan
            ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 0
            curr_VCO = ezca['SQZ-FC_VCO_TUNEOFS']
            time.sleep(0.2)
            Q_min = find_min(scan(curr_VCO-self.scanWidth,curr_VCO+self.scanWidth))

            if (Q_min < self.threshHigh):
                log(f'find_min wfs_q dip {Q_min} < {self.threshHigh}')
                ezca['SQZ-FC_VCO_TUNEOFS']-=0.05 #vx 3/12
                if FC2_M3_saturated(): #5/4 - check for FC2 M3 saturations before returning True
                    return 'DOWN'
                return True
            else:
                self.idx = (self.idx+1)%len(fcgsOffsetList) #try the other one   ###4/7
                self.counter += 1
                log('did not find rlf here, going to next offset')

class FINE_TUNE_IR(GuardState):
    request = False
    index = 240

    @GR_lock_checker
    #@TTFSS_checker
    def main(self):
        self.IRChan = 'SQZ-FC_WFS_A_Q_SUM_OUTPUT'

        self.fine_tune = 'volts'
        if self.fine_tune == 'volts':
            ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 0
            self.SweepChan = 'SQZ-FC_VCO_TUNEOFS' ##to sweep volts
            self.SweepWidth = 0.05 #SweepWidth  # width of the sweep is +/- this number
            self.StepSize = 0.0005 #StepSize
        elif self.fine_tune == 'freq':
            ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 1
            self.SweepChan = 'SQZ-FC_VCO_CONTROLS_SETFREQUENCYOFFSET'
            self.SweepWidth = 500 #SweepWidth  # width of the sweep is +/- this number
            self.StepSize = 5 #StepSize

        self.threshHigh = -100   # threshold on transmitted power to jump to next state
        self.SleepTime = 0.15 #SleepTime #time to pause for each step (seconds)
        self.NumSteps = 2*math.floor(self.SweepWidth/self.StepSize) # total number of steps
        self.counter = 0
        self.direction = 1 # positive or negative
        time.sleep(1)
        self.StartPoint = ezca[self.SweepChan]
        self.irValueOld = ezca[self.IRChan]
        self.irValueNew = ezca[self.IRChan]
        self.skip = False
        self.timer['pause'] = 3
        self.timer['timeout'] = 90

        if FC2_M3_saturated(): #5/4 - check for FC2 M3 saturations
            log('saturated FC2 M3')
            return 'DOWN'

    @GR_lock_checker
    #@TTFSS_checker
    def run(self):
        if self.skip == True:
            return True
        self.SleepTime = 0.15
        self.irValueNew = ezca[self.IRChan]
        log('WFS_A Q dip is {}'.format(self.irValueNew))

        if self.irValueNew < self.threshHigh:
            log('IR found at {}'.format(ezca[self.SweepChan]))
            return True

        elif self.counter < self.NumSteps:
            ezca[self.SweepChan] += self.StepSize * self.direction
            self.irValueOld = self.irValueNew
            time.sleep(self.SleepTime)
            self.counter += 1

        elif self.counter >= self.NumSteps:
            ezca[self.SweepChan] = self.StartPoint
            time.sleep(0.5)
            self.direction *= -1
            self.counter = 0

        if self.timer['timeout']:
            return 'GR_LOCKED'



class SAVE_OFFSET(GuardState):
    index = 270
    request = False

    @GR_lock_checker
    def main(self):
        #self.NewOffset = ezca['SQZ-FC_VCO_CONTROLS_SETFREQUENCYOFFSET']
        self.NewOffset = np.round(ezca['SQZ-FC_VCO_TUNEOFS'],3)
        self.Tolerance = 0.5 #2000
        self.savedOffsetList = fcParamDict['vcoOffsets'] #fcParamDict['fcOffsets']
        log(self.savedOffsetList)
        self.writer = 0
        log('found at offset {}'.format(self.NewOffset))

        #write offset to fcparams here
        for ii in range(0, len(self.savedOffsetList)):
            log('checking against saved offset #{} whose value is {}'.format(ii, self.savedOffsetList[ii]))
            if (abs(self.NewOffset - self.savedOffsetList[ii]) < self.Tolerance): #within tolerance
                if (abs(self.NewOffset - self.savedOffsetList[ii]) > 5e-3): #but w/in some amount
                    if self.writer < 1:
                        log('Replacing saved offset of {} with new offset of {}.'.format(self.savedOffsetList[ii], self.NewOffset))
                        self.savedOffsetList[ii] = self.NewOffset
                        # Write the new parameters to the file
                        #fcParamDict['fcOffsets'] = fcgsOffsetList
                        fcParamDict['vcoOffsets'] = self.savedOffsetList
                        with open(fcParamsPath, 'w+') as fcParamFile:
                            json.dump(fcParamDict, fcParamFile)
                        self.writer = 1
                        return True
                else: log('within 5mV of known offsets')
            else:
                log('not saving offset here, too different from existing offsets')

class IR_FOUND(GuardState):
    index = 350
    request = True

    def main(self):
        ezca['SQZ-FC_VCO_CONTROLS_ENABLE'] = 0

    #@TTFSS_checker
    @GR_lock_checker
    def run(self):
        return True


class TRANSITION_IR_LOCKING(GuardState):
    index = 500
    request = False

    def main(self):
        self.TRANSITION_GR_TO_IR_TRAMP = 2
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = self.TRANSITION_GR_TO_IR_TRAMP
        ezca['SQZ-FC_LSC_DOF2_GAIN'] = 1
        ezca['SQZ-FC_LSC_OUTMTRX_2_2'] = 1  # enable outmatrix DOF2 --> FC2
        ezca.switch('SQZ-FC_LSC_DOF2','INPUT','ON','OUTPUT','ON')

        self.IR_GAIN = sqzparams.fc_wfs_a_ir_gain #in sqzparams.py
        #self.GR_GAIN = ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6']
        self.GR_GAIN = sqzparams.fc_green_sus_gain

        self.counter = 1
        #self.counter = 0

    @GR_lock_checker
    def run(self):

        if not ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH']:
            return 'GR_LOCKED'

        #if self.counter == 0:
            #if ezca['SQZ-FC_WFS_A_Q_SUM_OUTPUT'] > 1000:
                #return 'GR_LOCKED'

        if self.counter == 1: # turn on IR gain, slowly ramp off GR FC-LSC
            ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = np.round(self.GR_GAIN*0.5,2)
            ezca['SQZ-FC_LSC_INMTRX_SETTING_2_7'] = self.IR_GAIN
            ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
            self.timer['pause'] = self.TRANSITION_GR_TO_IR_TRAMP
            self.counter += 1

        elif self.counter == 2 and self.timer['pause']: # totally off GR FC-LSC
            ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = 0
            ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
            self.timer['pause'] = self.TRANSITION_GR_TO_IR_TRAMP+1
            self.counter += 1

        elif self.counter == 3 and self.timer['pause']:
            ezca['SQZ-FC_LSC_DOF1_TRAMP'] = self.TRANSITION_GR_TO_IR_TRAMP
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0
            self.timer['pause'] = 3
            self.counter += 1

        #elif self.counter == 4 and self.timer['pause']:
            # disable + reset FCGS CMB servo
        #    ezca['SQZ-FC_SERVO_COMBOOST'] = 0
        #    ezca['SQZ-FC_SERVO_IN1EN'] = 'Off'
        #    self.timer['pause'] = 1
        #    self.counter += 1

        elif self.counter == 4 and self.timer['pause']:
            return True


class FC_ASC_ON(GuardState):
    index = 750
    request = False

    def main(self):
        for py in ['PIT', 'YAW']:
            for dof in range(2):
                # Turning on dithers
                ezca['SQZ-FC_ASC_ADS_%s%s_OSC_CLKGAIN'%(py,dof+1)] = 10
        
        self.counter = 0
        self.timer['pause'] = 0

        self.use_beamspot_control = sqzparams.use_fc_beamspot_control  #True
        ezca.get_LIGOFilter('SQZ-FC_ASC_INJ_ANG_P').switch_off('INPUT')
        ezca.get_LIGOFilter('SQZ-FC_ASC_INJ_ANG_Y').switch_off('INPUT')

    def run(self):
        if not sqzparams.use_fc_asc:
            return True

        elif self.counter == 0:
            log('IR is locked, engaging FC ASC')
            ezca['SQZ-FC_ASC_SWITCH'] = 1
            self.timer['pause'] = 20
            self.counter += 1

        elif self.counter == 1 and self.timer['pause']:
            if not ezca['SQZ-FC_ASC_TRIGGER_MON']:
                notify('FC ASC not on!! Check FC ASC trigger.')
            if self.use_beamspot_control:
                ezca.get_LIGOFilter('SQZ-FC_ASC_INJ_ANG_P').switch_on('INPUT')
                ezca.get_LIGOFilter('SQZ-FC_ASC_INJ_ANG_Y').switch_on('INPUT')
            self.timer['pause'] = 5
            self.counter += 1

        elif self.counter == 2 and self.timer['pause']:
            return True


class IR_LOCKED(GuardState):
    index = 1000
    request = True
    #@GR_lock_checker
    def run(self):
        if IR_locked():
            if sqzparams.use_fc_asc:
                if ezca['SQZ-FC_ASC_CAV_ANG_P_INMON']==0 and ezca['SQZ-FC_ASC_CAV_ANG_Y_INMON']==0:
                    notify('FC ASC not on??')
            return True
        else:
            notify('IR unlocked?')
            return 'DOWN'

#################################################################################
########  LOCK FC USING SQZ LASER VCO -- see alog 66380 for re-cabling  #########
###############  ** not sure if these states work yet.. ** ######################
#################################################################################
class IR_VCO_LOCKING(GuardState):
    index = 25
    request = False

    def main(self):
        self.TIMEOUT_T = 0.25
        self.ir_lockAttempt_counter = 0

        self.ir_res_tunevolts = [-2.4, 3.5]

        # FCGS CMB FAST output should be cabled to SQZ LASER VCO TUNE input, instead of FCGS VCO tune
        ezca['SQZ-FC_SERVO_COMBOOST'] = 0
        ezca['SQZ-FC_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-FC_SERVO_IN1GAIN'] = 0
        ezca['SQZ-FC_SERVO_FASTGAIN'] = 0
        ezca['SQZ-FC_SERVO_IN1POL'] = 'On'   #FOR IR VCO
        ezca['SQZ-FC_SERVO_FASTPOL'] = 'Off' #FOR IR VCO
        ezca['SQZ-FC_SERVO_IN1EN'] = 'Off'

        # use trigger-controlled FC_LSC_DOF3 for Laser AOM feedback
        ezca['SQZ-FC_LSC_SWITCH'] = 1 #enable trigger for dof 3,4 filter banks
        ezca['SQZ-FC_LSC_GAIN'] = 1   #trigger gain
        ezca['SQZ-FC_LSC_DOF3_GAIN'] = 0
        ezca.get_LIGOFilter('SQZ-FC_LSC_DOF3').only_on('FM1','FM2','FM3','FM8','FM9','FM10', 'INPUT','OUTPUT','DECIMATION')

        # FC_LSC output to laser AOM VCO is summed with FC_LSC dither lock.
        # To use Laser AO feedback, enable "SQZ-FC_LSC_FREQ" filter bank with gain of 1.
        ezca['SQZ-FC_LSC_FREQ_GAIN'] = 1
        ezca.get_LIGOFilter('SQZ-FC_LSC_FREQ').only_on('INPUT','OUTPUT','DECIMATION')

        # reset outmatrix for laser AO
        for ii in range(3):
            for jj in range(4):
                ezca['SQZ-FC_LSC_OUTMTRX_%d_%d'%(ii+1,jj+1)] = 0
        ezca['SQZ-FC_LSC_OUTMTRX_3_3'] = 1  #DOF3 --> Laser AO
        ezca.get_LIGOFilter('SUS-FC2_M3_LOCK_L').switch_off('INPUT')

        #configure inmatrix to send WFS_A_I to DOF 3
        ezca['SQZ-FC_LSC_DOF3_GAIN'] = 1.5
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 1

        self.scanWidth = 0.2
        self.threshHigh = -500

        ezca['SQZ-FC_VCO_TUNEOFS'] = self.ir_res_tunevolts[0]
        self.timer['pause']=0.5
        self.timer['timeout']=60
        self.counter = 0

    def run(self):
        if self.counter == 0 and self.timer['pause']:
            ezca['SQZ-FC_SERVO_IN1EN'] = 'On'
            self.timer['pause'] = 5
            self.counter += 1

        elif self.counter == 1:
            if GR_locked():
                notify('gr locked, boosting')
                ezca['SQZ-FC_SERVO_COMBOOST'] = 1
                self.counter += 1
            elif not GR_locked() and self.timer['pause']:
                log('timeout... Toggling servo.')
                ezca['SQZ-FC_SERVO_COMBOOST'] = 0
                ezca['SQZ-FC_SERVO_IN1EN'] = 'Off' # toggle off
                self.timer['pause'] = 1
                self.counter = 0

        elif self.counter == 2:
            if GR_locked():
                log('scanning ir')
                curr_VCO = ezca['SQZ-FC_VCO_TUNEOFS']
                Q_min = find_min(scan(curr_VCO-self.scanWidth,curr_VCO+self.scanWidth,volts_stepSize=0.005))
                if (Q_min < self.threshHigh):
                    log('q min = %f'%(Q_min))
                    log(f'find_min wfs_q dip {Q_min} < {self.threshHigh}')
                    ezca['SQZ-FC_LSC_INMTRX_SETTING_3_7']=-1
                    ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1  #engage laser aom servo
                    return True
                else:
                    self.ir_lockAttempt_counter += 1
                    ezca['SQZ-FC_VCO_TUNEOFS'] = self.ir_res_tunevolts[(self.ir_lockAttempt_counter)%2]
                    if self.ir_lockAttempt_counter > len(self.ir_res_tunevolts):
                        return 'DOWN'
            else: self.counter = 0

        if self.timer['timeout']==60:
            return 'DOWN'

class IR_VCO_LOCKED(GuardState):
    index = 30
    request = False
    @GR_lock_checker
    def run(self):
        return True



'''
class TRANSITION_GR_LOCKING(GuardState):
    index = 503
    request = False

    @GR_lock_checker
    def main(self):
        self.TRANSITION_GR_TO_IR_TRAMP = 2
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = self.TRANSITION_GR_TO_IR_TRAMP
        ezca['SQZ-FC_LSC_DOF1_RSET'] = 2
        ezca.get_LIGOFilter('SQZ-FC_LSC_DOF1').ramp_gain(1, ramp_time=0, wait=False)
        self.timer['wait']=0
        self.counter = 0

    @GR_lock_checker
    def run(self):
        if self.counter == 0:  #first turn green sus feedback back on
            ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = 1
            ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
            self.timer['wait']=self.TRANSITION_GR_TO_IR_TRAMP
            self.counter += 1
        elif self.counter == 1 and self.timer['wait']:  #then turn on green lsc
            ezca['SQZ-FC_LSC_INMTRX_SETTING_2_7'] = 0
            ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
            ezca.get_LIGOFilter('SQZ-FC_LSC_DOF2').ramp_gain(0, ramp_time=5, wait=self.TRANSITION_GR_TO_IR_TRAMP)
            self.timer['wait']=self.TRANSITION_GR_TO_IR_TRAMP
            self.counter += 1
        elif self.counter == 2 and self.timer['wait'] and GR_locked():
            return True
'''

class FC_MISALIGNED(GuardState):
    index = 1
    request = True

    def main(self):
        nodes['SUS_FC2'] = 'MISALIGNED'
    def run(self):
        if nodes['SUS_FC2'].arrived and nodes['SUS_FC2'].done:
            return True


edges = [
    ('INIT','IDLE'),
    ('DOWN','DOWN'),
    ('DOWN','FC_MISALIGNED'),
    ('DOWN','OFFLOAD_FC_ASC'),
    ('OFFLOAD_FC_ASC','FC_ASC_OFFLOADED'),
    ('DOWN','IDLE'),
    ('IDLE','GR_VCO_LOCKING'),
    ('GR_VCO_LOCKING','GR_SUS_LOCKING'),
    ('GR_SUS_LOCKING','GR_LOCKED'),
    ('GR_LOCKED','FIND_IR'),
    ('FIND_IR','FINE_TUNE_IR'),
    ('FINE_TUNE_IR', 'SAVE_OFFSET'),
    ('SAVE_OFFSET', 'IR_FOUND'),
    ('IR_FOUND','GR_LOCKED'),
    ('IR_FOUND','TRANSITION_IR_LOCKING'),
    ('TRANSITION_IR_LOCKING','FC_ASC_ON'),
    ('FC_ASC_ON','IR_LOCKED'),
    #('IR_LOCKED','TRANSITION_GR_LOCKING'),
    #('TRANSITION_GR_LOCKING','GR_LOCKED'),
    ('IDLE','IR_VCO_LOCKING'),
    ('IR_VCO_LOCKING','IR_VCO_LOCKED'),
    #('IR_LOCKED','OFFLOAD_FC_ASC'),
    #('OFFLOAD_FC_ASC','IR_LOCKED'),
]

